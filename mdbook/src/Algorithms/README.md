## Intro to Algorithms

### **Introduction:**
In this section, the Student will learn to implement complex data structures and algorithms using Python

Algorithms and Data structures allow you to store and organize data efficiently. 
They are critical to any problem, provide robust solutions, and facilitate reusable code. 
Overall, this Introduction to Algorithms with Python will teach you the most essential and most common algorithms.

### **Topics Covered:**

* [**Algorithms Analysis**](Algorithm_Analysis.html)
* [**Algorithms Design**](Algorithm_Design.html)
* [**Big O Notation**](Big_O_notation.html)
* [**Searching Algorithms**](Searching_Algorithms.html)
* [**Sorting Algorithms**](Sort.html)


#### To access the Algorithms slides please click [here](slides/)
