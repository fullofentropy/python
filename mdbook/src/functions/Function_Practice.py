"""
Function Problems

1. Kilometer Converter
Write a program that asks the user to enter a distance in kilometers, and then converts that
distance to miles. The conversion formula is as follows:
    Miles = Kilometers * 0.6214


2. How Much Insurance?
Many financial experts advise that property owners should insure their homes or buildings for at least 80 percent of the
amount it would cost to replace the structure. Write a
program that asks the user to enter the replacement cost of a building and then displays
the minimum amount of insurance he or she should buy for the property

3. Automobile Costs
Write a program that asks the user to enter the monthly costs for the following expenses
incurred from operating his or her automobile: loan payment, insurance, gas, oil, tires, and
maintenance. The program should then display the total monthly cost of these expenses,
and the total annual cost of these expenses.

4. Property Tax
A county collects property taxes on the assessment value of property, which is 60 percent of the property’s actual value. For
example, if an acre of land is valued at $10,000,
its assessment value is $6,000. The property tax is then 64¢ for each $100 of the assessment value. The tax for the acre
assessed at $6,000 will be $38.40. Write a program that
asks for the actual value of a piece of property and displays the assessment value and
property tax.

5. Body Mass Index
Write a program that calculates and displays a person’s body mass index (BMI). The
BMI is often used to determine whether a person is overweight or underweight for his
or her height. A person’s BMI is calculated with the following formula:
    BMI = weight * 703 / height^2
where weight is measured in pounds and height is measured in inches

6. Calories from Fat and Carbohydrates
A nutritionist who works for a fitness club helps members by evaluating their diets. As part
of her evaluation, she asks members for the number of fat grams and carbohydrate grams
that they consumed in a day. Then, she calculates the number of calories that result from
the fat, using the following formula:
    calories from fat = fat grams * 9
Next, she calculates the number of calories that result from the carbohydrates, using the
following formula:
    calories from carbs = carb grams * 4
The nutritionist asks you to write a program that will make these calculations.

7. Stadium Seating
There are three seating categories at a stadium. For a softball game, Class A seats cost $15,
Class B seats cost $12, and Class C seats cost $9. Write a program that asks how many tickets for each class of seats were
sold, and then displays the amount of income generated from
ticket sales.


8. Paint Job Estimator
A painting company has determined that for every 115 square feet of wall space, one gallon of paint and eight hours of labor
will be required. The company charges $20.00 per
hour for labor. Write a program that asks the user to enter the square feet of wall space to
be painted and the price of the paint per gallon. The program should display the following
data:
• The number of gallons of paint required
• The hours of labor required
• The cost of the paint
• The labor charges
• The total cost of the paint job

9. Monthly Sales Tax
A retail company must file a monthly sales tax report listing the total sales for the month,
and the amount of state and county sales tax collected. The state sales tax rate is 4 percent
and the county sales tax rate is 2 percent. Write a program that asks the user to enter the
total sales for the month. From this figure, the application should calculate and display the
following:
• The amount of county sales tax
• The amount of state sales tax
• The total sales tax (county plus state)


"""