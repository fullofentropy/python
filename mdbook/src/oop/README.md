## Object Oriented Programming

### **Introduction:**
This lesson you will walk through Object-oriented Programming, or OOP for short, which bundles attributes and methods into individual objects. Here you will learn how to create and use objects using Object-Oriented Programming principals.

### **Topics Covered:**

* [**Modules**](modules.html#modules)
* [**Packages**](packages.html#packages)
* [**User Classes Pt. 1**](user_classes.html#user-classes-pt1)
* [**User Classes Pt.2**](user_classes_pt2.html#user-classes-pt2)
* [**Composition vs Inheritance**](user_classes_pt2.html#composition-vs-inheritance)
* [**Exception Handling**](exceptions.html#exceptions)
* [**Object Oriented Programming Principles**](oop_principles.html#oop-principles)
* [**Object Oriented Programming Terminology Review**](oop_terminology.html#oop-terminology-review)


#### To access the Object Oriented Programming slides please click [here](slides)

